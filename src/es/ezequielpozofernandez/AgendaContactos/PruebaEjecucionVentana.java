/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ezequielpozofernandez.AgendaContactos;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Luffy
 */
public class PruebaEjecucionVentana extends Application {

    private EntityManagerFactory emf;
    private EntityManager em;
    ContactosViewController contactosViewController;

    @Override
    public void start(Stage primaryStage) throws IOException {
       
        StackPane rootMain = new StackPane();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Ventana.fxml"));
        Pane rootContactosView = fxmlLoader.load();
        rootMain.getChildren().add(rootContactosView);

        emf = Persistence.createEntityManagerFactory("AgendaContactosPU");
        em = emf.createEntityManager();
        contactosViewController = (ContactosViewController) fxmlLoader.getController();
        contactosViewController.setEntityManager(em);
        contactosViewController.cargarTodasPersonas();

        Scene scene = new Scene(rootMain, 700, 400);

        primaryStage.setTitle("Agenda Contactos");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        em.close();
        emf.close();
        try {
            DriverManager.getConnection("jdbc:derby:BDAgendaContactos;shutdown=true");
        } catch (SQLException ex) {
        }
    }



    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
