/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ezequielpozofernandez.AgendaContactos;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Luffy
 */
public class AgendaContactos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AgendaContactosPU");
        EntityManager em = emf.createEntityManager();

        //Creacion de provincias
        //Provincia provinciaCadiz = new Provincia(0, "Cádiz");
        //Provincia provinciaSevilla = new Provincia();
        //provinciaSevilla.setNombre("Sevilla");
        em.getTransaction().begin(); //para comenzar las transaciones 
//        em.persist(provinciaCadiz);
//        em.persist(provinciaSevilla);

        //Modificacion de objetos
        Query queryProvinciaCadiz = em.createNamedQuery("Provincia.findByNombre");
        queryProvinciaCadiz.setParameter("nombre", "Cádiz");
        List<Provincia> listProvinciasCadiz = queryProvinciaCadiz.getResultList();
        for (Provincia provinciaCadiz : listProvinciasCadiz) {
            provinciaCadiz.setCodigo("11");
            em.merge(provinciaCadiz);
        }
        
        Provincia provinciaId2 = em.find(Provincia.class, 2);
        if (provinciaId2 != null) {
            em.remove(provinciaId2);
        } else {
            System.out.println("No hay ninguna provincia con ID=2");
        }

        em.getTransaction().commit();

        //  em.getTransaction().rollback(); //Para cancelar el commit
        em.close();
        emf.close();
        try {
            DriverManager.getConnection("jdbc:derby:BDAgendaContactos;shutdown=true");
        } catch (SQLException ex) {

        }

    }
}
