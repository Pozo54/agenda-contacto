/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ezequielpozofernandez.AgendaContactos;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Luffy
 */
public class ConsultaProvincia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AgendaContactosPU");
        EntityManager em = emf.createEntityManager();

        Query queryProvincias = em.createNamedQuery("Provincia.findAll");
        List<Provincia> listProvincias = queryProvincias.getResultList();

        //Diferentes maneras, mismo resultado.
        for (Provincia provincia : listProvincias) {
            System.out.println(provincia.getNombre());
        }

//        for (int i = 0; i < listProvincias.size(); i++) {
//            Provincia provincia = listProvincias.get(i);
//            System.out.println(provincia.getNombre());
//        }
//Mostrara un determinado objeto, en este caso cadiz.
//(Se le ha cambiado el nombre a la variable para que no de fallos)
        Query queryProvinciaCadiz = em.createNamedQuery("Provincia.findByNombre");
        queryProvinciaCadiz.setParameter("nombre", "Cádiz");
        List<Provincia> listProvinciasCadiz = queryProvinciaCadiz.getResultList();
        for (Provincia provinciaCadizz : listProvinciasCadiz) {
            System.out.print(provinciaCadizz.getId() + ": ");
            System.out.println(provinciaCadizz.getNombre());
        }
        
        //Ejecutar cuyo ID sea 2
        Provincia provinciaId2 = em.find(Provincia.class, 2);
        if (provinciaId2 != null) {
            System.out.print(provinciaId2.getId() + ": ");
            System.out.println(provinciaId2.getNombre());
        } else {
            System.out.println("No hay ninguna provincia con ID=2");
        }
    }

}
